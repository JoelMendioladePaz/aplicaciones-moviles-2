package com.example.notificacionesupt.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.notificacionesupt.Api.Api;
import com.example.notificacionesupt.R;
import com.example.notificacionesupt.Servicios.servicioPeticion;
import com.example.notificacionesupt.ViewModels.Peticion_Login;
import com.example.notificacionesupt.ViewModels.Peticion_Noticia;
import com.example.notificacionesupt.ViewModels.Peticion_crear_noticia;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Noticias extends AppCompatActivity {
    private EditText Titulo;
    private EditText Descripcion;
    private Button Enviarnoticia;
    private int id=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        getSupportActionBar().hide();
        Titulo=(EditText) findViewById(R.id.edtnoticiastitulo);
        Descripcion=(EditText) findViewById(R.id.edtnoticiasdescripcion);
        Enviarnoticia=(Button) findViewById(R.id.btnenviarnoticia);

        servicioPeticion service = Api.getApi(Noticias.this).create(servicioPeticion.class);
        Call<Peticion_Noticia> peticion_noticiaCall= service .getNoticias();
        peticion_noticiaCall.enqueue(new Callback<Peticion_Noticia>() {
            @Override
            public void onResponse(Call<Peticion_Noticia> call, Response<Peticion_Noticia> response) {
                Peticion_Noticia peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(Noticias.this,"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.estado == "true"){
                    //startActivity(new Intent(Login.this, Login.class));
                    for (int i=0;i<peticion.detalle.size()-1;i++){
                        Toast.makeText(Noticias.this,peticion.detalle.get(i).titulo,Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(Noticias.this,peticion.estado,Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Peticion_Noticia> call, Throwable t) {
                Toast.makeText(Noticias.this,"No podemos conectarnos :c",Toast.LENGTH_SHORT).show();
            }
        });
        Enviarnoticia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(Titulo.getText().toString())){
                    Titulo.setError("Este campo no puede estar vacio");
                }
                if (TextUtils.isEmpty(Descripcion.getText().toString())){
                    Descripcion.setError("Este campo no puede estar vacio");
                }else {
                    id= (int) Math.random()*270+5;
                    servicioPeticion service = Api.getApi(Noticias.this).create(servicioPeticion.class);
                    Call<Peticion_crear_noticia> peticion_crear_noticiaCall= service .Crear_noticia(id,Titulo.getText().toString(),Descripcion.getText().toString());
                    peticion_crear_noticiaCall.enqueue(new Callback<Peticion_crear_noticia>() {
                        @Override
                        public void onResponse(Call<Peticion_crear_noticia> call, Response<Peticion_crear_noticia> response) {
                            Peticion_crear_noticia peticion=response.body();
                            if(response.body()==null){
                                Toast.makeText(Noticias.this,"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if(peticion.estado == "true"){
                                //startActivity(new Intent(Login.this, Login.class));

                                    Toast.makeText(Noticias.this,"Se regristro exitosamente",Toast.LENGTH_SHORT).show();

                            }else{
                                Toast.makeText(Noticias.this,peticion.detalle,Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<Peticion_crear_noticia> call, Throwable t) {
                            Toast.makeText(Noticias.this,"No podemos conectarnos :c",Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }
}
