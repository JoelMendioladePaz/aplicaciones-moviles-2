package com.example.notificacionesupt.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.notificacionesupt.Api.Api;
import com.example.notificacionesupt.R;
import com.example.notificacionesupt.Servicios.servicioPeticion;
import com.example.notificacionesupt.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {

    private TextView Login;
    private Button Registrarse;
    private EditText Correo;
    private EditText Contrasenia;
    private EditText repetirContrasenia;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regristro);
        getSupportActionBar().hide();
        Registrarse =(Button) findViewById(R.id.btnRegistrarme);
        Correo=(EditText) findViewById(R.id.edcorreoRegistro);
        Contrasenia=(EditText) findViewById(R.id.edcontraseniaRegistro);
        repetirContrasenia=(EditText) findViewById(R.id.edrepetirContraseniaRegistro);
        Login=(TextView) findViewById(R.id.txtvcrearcuenta2);

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Login = new Intent(Registro.this, com.example.notificacionesupt.Activities.Login.class);
                startActivity(Login);
            }
        });

        Registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(Correo.getText().toString()))
                    Correo.setError("Este campo no puede estar vacio");
                if (TextUtils.isEmpty(Contrasenia.getText().toString()))
                    Contrasenia.setError("Este campo no puede estar vacio");
                if (TextUtils.isEmpty(repetirContrasenia.getText().toString()))
                    repetirContrasenia.setError("Este campo no puede estar vacio");
                else if (!TextUtils.equals(Contrasenia.getText(),repetirContrasenia.getText()))
                    repetirContrasenia.setError("La contraseña no coincide");
                else{
                    servicioPeticion service = Api.getApi(Registro.this).create(servicioPeticion.class);
                    Call<Registro_Usuario> registro_usuarioCall= service .registrarUsuario(Correo.getText().toString(),Contrasenia.getText().toString());
                    registro_usuarioCall.enqueue(new Callback<Registro_Usuario>() {
                        @Override
                        public void onResponse(Call<Registro_Usuario> call, Response<Registro_Usuario> response) {
                            Registro_Usuario peticion=response.body();
                            if(response.body()==null){
                                Toast.makeText(Registro.this,"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if(peticion.estado == "true"){
                                startActivity(new Intent(Registro.this, com.example.notificacionesupt.Activities.Login.class));
                                Toast.makeText(Registro.this,"Datos Registrados",Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(Registro.this,peticion.detalle,Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void onFailure(Call<Registro_Usuario> call, Throwable t) {
                            Toast.makeText(Registro.this,"No podemos conectarnos :c",Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

    }
}
