package com.example.notificacionesupt.Servicios;

import com.example.notificacionesupt.ViewModels.Peticion_Login;
import com.example.notificacionesupt.ViewModels.Peticion_Noticia;
import com.example.notificacionesupt.ViewModels.Peticion_crear_noticia;
import com.example.notificacionesupt.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface servicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario>registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> login(@Field("username") String correo, @Field("password") String contrasenia);


    @GET("api/todasNot")
    Call<Peticion_Noticia> getNoticias();

    @FormUrlEncoded
    @POST("api/crearNotUsuario")
    Call<Peticion_crear_noticia> Crear_noticia(@Field("usuarioId") int id, @Field("titulo") String titulo,@Field("descripcion") String descripcion);

}
