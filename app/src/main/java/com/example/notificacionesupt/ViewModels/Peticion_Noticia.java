package com.example.notificacionesupt.ViewModels;

import java.util.List;

public class Peticion_Noticia {
    public String estado;
    public List<Detalle> detalle;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Detalle> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<Detalle> detalle) {
        this.detalle = detalle;
    }


}

