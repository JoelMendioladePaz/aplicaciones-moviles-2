package com.example.notificacionesupt.ViewModels;

public class Peticion_crear_noticia {
    public String estado;
    public int usuarioId;
    public String titulo;
    public String descricion;
    public String detalle;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
}
